'use strict';

const events = require('../utils/custom-events');
const preferences = require('../utils/user-preferences');
const SOUNDS = require('../utils/sounds');

// this array will be MenuItems thus the mapping is to add defaults values
const SOUND_ITEMS = SOUNDS.map(function (sound, index) {
  sound.type = 'checkbox';
  sound.checked = preferences.notificationSound === index;
  sound.click = function () {
    preferences.notificationSound = index;
  };
  return sound;
});

// update the checked sounds on change
events.on('preferences:change:notificationSound', function () {
  SOUND_ITEMS.forEach(function (sound, index) {
    sound.checked = preferences.notificationSound === index;
  });
});

module.exports = SOUND_ITEMS;
